<?php

class TinodeAdmin {

  /**
   * Register settings for Tinode and create settings sections and fields
   */
  static function admin_init() {
    // register the backend url setting
    register_setting('tinode', 'tinode_backend_url', array(
      'type' => 'string', 'sanitize_callback' => 'sanitize_text_field'
    ));

    // create the settings section
    add_settings_section('tinode_admin_section', "Tinode Chat Server", function () {
      ?>
<p>You must host your own Tinode Chat (https://github.com/tinode/chat) server</p>
<p>Note: This plugin will not do anything for you regarding CORS. It will only work out-of-the-box when your Tinode Chat server is on the same domain as your Wordpress.</p>
<p>Once you have it up and running, and configured json-rpc, you should configure the connection settings to your server here</p><?php
    }, 'tinode');

    // create form inputs and attach them to the section
    // Tinode chat server URL
    add_settings_field('tinode_admin_field_backend_url', "Tinode Full URL", function () {
      $setting = get_option('tinode_backend_url');
      ?>
<input type="text" name="tinode_backend_url" value="<?php echo isset($setting) ? esc_attr($setting) : ''; ?>" />
<p>Example: https://my.tinode.com:6060</p><?php
    }, 'tinode', 'tinode_admin_section');

    add_settings_field('tinode_admin_field_api_key', "Wordpress Tinode API Key", function() {
      $setting = get_option('tinode_plugin_api_key');
      ?>
<input type="password" name="tinode_plugin_api_key" value="<?php echo isset($setting) ? esc_attr($setting) : ''; ?>" />
<p>Secret API Key for Tinode to authenticate with Wordpress</p>
<p>Application Passwords are available: <?php echo wp_is_application_passwords_available() ? "yes" : "no"; ?></p>
<p>SSL is enabled: <?php echo is_ssl() ? "yes" : "no"; ?></p>
<p>Environment type is: <?php echo wp_get_environment_type(); ?></p><?php
    }, 'tinode', 'tinode_admin_section');
  }

  /**
   * Draw the settings menu item and page based on sections defined above
   */
  static function admin_menu() {
    add_menu_page("Tinode", "Tinode", 'manage_options', 'tinode', function () {
      // check permissions
      if (!current_user_can('manage_options')) return;

      // create and draw all messages on the page
      self::add_settings_messages();

      // display the form which will manage the settings
      ?><form action="options.php" method="post"><?php

      // security fields like nonce etc
      settings_fields('tinode');

      // render all sections at this point in the page
      do_settings_sections('tinode');

      submit_button("Save");

      // close the form tag
      ?></form><?php
    });
  }

  /**
   * Draw page messages
   */
  static function add_settings_messages() {
    // when settings are updated ..
    if (isset($_GET['settings-updated'])) {
      // .. display success message
      add_settings_error('tinode_settings_messages', 'tinode_message_updated', "Saved", 'success');
    }

    $backend_url = get_option('tinode_backend_url');

    if (isset($backend_url)) {
      if ($backend_url != "" && !TinodeBackend::is_reachable()) {
        // display an additional error message if the backend is unreachable
        add_settings_error('tinode_settings_messages', 'tinode_message_backend_unreachable', "Cannot reach Tinode Server at {$backend_url}");
      }
    }

    // display all messages
    settings_errors('tinode_settings_messages');
  }

}
