<?php

class TinodeConfig {

  static function activate_plugin() {
    add_option('tinode_plugin_enabled', true);

    # https://make.wordpress.org/core/2020/11/05/application-passwords-integration-guide/
    $some_previous_api_key = get_option('tinode_plugin_api_key');
    if (!isset($some_previous_api_key)) {
      add_option('tinode_plugin_api_key', wp_generate_password());
    }
  }

  static function deactivate_plugin() {
    add_option('tinode_plugin_enabled', false);
  }

  static function uninstall_plugin() {
    delete_option('tinode_plugin_enabled');
    delete_option('tinode_plugin_api_key');
  }

  static function plugin_is_active() {
    return get_option('tinode_plugin_enabled');
  }

}
