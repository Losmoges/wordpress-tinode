<?php

class TinodeBackend {

  /**
   * Calls the /stats/expvar monitoring endpoint at some url to test if it responds with some expected information.
   * In case that it does, expect that this is a Tinode chat server and is up & running
   */
  static function is_reachable() {
    $url = get_option('tinode_backend_url');
    $remote_get = wp_remote_get("{$url}/stats/expvar");

    if (is_wp_error($remote_get)) {
      return false;
    }

    $body = wp_remote_retrieve_body($remote_get);
    $json = json_decode($body);

    if (isset($json)) {
      if (property_exists($json, 'Uptime')) {
        return $json->Uptime > 0;
      }
    }

    return false;
  }

}
