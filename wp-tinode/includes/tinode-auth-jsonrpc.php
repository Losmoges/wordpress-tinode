<?php

/**
 * JSON-RPC authentication endpoint, so Tinode can delegate user management to Wordpress
 * https://github.com/tinode/chat/tree/master/server/auth/rest
 */
class TinodeAuthJsonRPC {

  // This REST API is available at /?rest_route=/tinode/auth/jsonrpc/v1
  // Or alternatively /wp-json/tinode/auth/jsonrpc/v1 if pretty permalinks is enabled
  // https://developer.wordpress.org/rest-api/key-concepts/
  private static $api_namespace = 'tinode/auth/jsonrpc/v1';

  /**
   * Hooked to define entrypoints. Modeled after the examples at https://github.com/tinode/chat/tree/master/rest-auth
   */
  static function rest_api_init() {
    register_rest_route(self::$api_namespace, '/health', array('methods' => 'GET', 'callback' => array('TinodeAuthJsonRPC', 'on_get_health')));

    register_rest_route(self::$api_namespace, '/auth', array(
      'methods' => 'POST', 'callback' => array('TinodeAuthJsonRPC', 'on_post_auth'), 'permission_callback' => array('TinodeAuthJsonRPC', 'permission_callback')
    ));

    // Unsupported endpoints. Wordpress is in control
    // Todo: auth
    register_rest_route(self::$api_namespace, '/add', array('methods' => 'POST', 'callback' => array('TinodeAuthJsonRPC', 'on_unsupported')));
    register_rest_route(self::$api_namespace, '/checkunique', array('methods' => 'POST', 'callback' => array('TinodeAuthJsonRPC', 'on_unsupported')));
    register_rest_route(self::$api_namespace, '/del', array('methods' => 'POST', 'callback' => array('TinodeAuthJsonRPC', 'on_unsupported')));
    register_rest_route(self::$api_namespace, '/gen', array('methods' => 'POST', 'callback' => array('TinodeAuthJsonRPC', 'on_unsupported')));
    register_rest_route(self::$api_namespace, '/upd', array('methods' => 'POST', 'callback' => array('TinodeAuthJsonRPC', 'on_unsupported')));
  }

  /**
   * Checks whether a request is authorized
   */
  static function permission_callback() {
    if (current_user_can(TinodeConfig::$auth_jsonrpc_capability)) {
      return true;
    }

    return new WP_Error('rest_user_cannot_tinode_jsonrpc', "Unauthorized", array('status' => rest_authorization_required_code()));
  }

  static function on_get_health() {
    return;
  }

  static function on_post_auth() {
    return array('err' => "malformed");
  }

  static function on_unsupported() {
    return array('err' => "unsupported");
  }

}
