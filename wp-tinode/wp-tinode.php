<?php
/**
 * Plugin Name
 *
 * @package           Tinode
 * @author            Gosse Mol
 * @license           GPL-2.0-or-later
 *
 * @wordpress-plugin
 * Plugin Name:       Wordpress Tinode Chat
 * Description:       Tinode chat integration for Wordpress
 * Version:           0.0.1
 * Requires at least: 5.7.2
 * Author:            Gosse Mol
 * License:           GPL v2 or later
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see http://www.gnu.org/licenses/gpl-2.0.txt
 *
 */

// Hacky way to check if this is called directly instead of included as wp plugin
if (!function_exists('add_action')) {
	echo "Hi there! I'm just a plugin, not much I can do when called directly.";
	exit;
}

// Configuration manager
require_once(plugin_dir_path(__FILE__) . "includes/tinode-config.php");

// Plugin lifecycle hooks
register_activation_hook(__FILE__, array('TinodeConfig', 'activate_plugin'));
register_deactivation_hook(__FILE__, array('TinodeConfig', 'deactivate_plugin'));
register_uninstall_hook(__FILE__, array('TinodeConfig', 'uninstall_plugin'));

if (TinodeConfig::plugin_is_active()) {

  if (is_admin()) {
    // Admin interface with backend configuration
    require_once(plugin_dir_path(__FILE__) . "includes/tinode-backend.php");
    require_once(plugin_dir_path(__FILE__) . "includes/tinode-admin.php");
    // Admin menu items lifecycle
    add_action('admin_init', array('TinodeAdmin', 'admin_init'));
    add_action('admin_menu', array('TinodeAdmin', 'admin_menu'));
  }

  // JSON-RPC auth endpoints, used by the Tinode server as the autorization authority
  require_once(plugin_dir_path(__FILE__) . "includes/tinode-auth-jsonrpc.php");
  add_action('rest_api_init', array('TinodeAuthJsonRPC', 'rest_api_init'));

  // Plugin

  // tinode-gateway - (optional) api for frontend
  // tinode-webview - frontend
  // tinode-backend - connector to tinode server
}
