# wordpress-tinode

Wordpress tinode plugin. Bring your own tinode server

## Features

* Connects to a Tinode Chat server to offer chat services
* Sync users in Wordpress with the Tinode database (Wordpress is the boss)
* Display chats on your website by embedding the Tinode Chat client
* Control who may chat with who through the api

## Development

* git
* docker
* docker-compose
* composer
