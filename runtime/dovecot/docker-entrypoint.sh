#!/bin/sh

set -o errexit

for useradd in $CREATE_USERS; do
  useradd "$useradd" || true
done

postconf -e "myhostname = $(hostname)"
postfix start
dovecot -F
